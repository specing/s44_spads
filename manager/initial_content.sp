separate;
-- SPADS manager -- Content management - adds initial content and defines content lists.
-- Copyright (C) 2015-2020 Fedja Beader
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.


-- Adding maps:
-- The first parameter is the name as it appears in the download system and lobby's Map: selector.
-- The second parameters are tags, see below.
-- The third parameter is the file name, take it from ~/.spring/maps
--   note: prd saves file names in lower case. Needed to figure out if a file sucessfully downloaded.
-- The fourth parameter is free-form comment.

-- Tags:
-- answer questions:
-- Is it land_only, land (mostly, with some naval), naval (mostly), naval_only?
-- Is it flat, hilly? (hilly means the gradient is large, not directly that terrain
--   changes in elevation. Or that most of the fighting grounds are covered in hills).
--   use both if you are unsure.
-- Is it appropriate for FFA? 3FFA? 4FFA?
-- Is it appropriate for s44? kp? tc? ...
tagmap : dynamic_hash_tables.table (string);

procedure add_content (changes_made : out boolean;
                       engine_dir : string;
                       name : string; tags : string; ofname : string; comment : in string)
is
	--game_data : constant string := "$PWD" & "/game_data";
	game_data : constant string := PWD & "/game_data";
	maps_dir  : constant string := game_data & "/maps";
	games_dir : constant string := game_data & "/games";

	-- PATH="$PATH:/opt/springrts.com/spring/104.0/bin"

	fname : constant string := strings.to_lower(ofname);
	fpath : constant string := maps_dir & "/" & fname;

	-- nice overnight fetch
	-- url="$1"
	-- printf "%s\n" "wget --limit-rate=100k -nc "$url" -O "$fname" --"
	procedure add_or_append_to_map (key : string; value_without_newline : string) is
	begin
		if dynamic_hash_tables.has_element(tagmap, key) then
			-- This only looks like Ada...
			dynamic_hash_tables.append(tagmap, key, ASCII.LF & value_without_newline);
		else
			dynamic_hash_tables.add(tagmap, key, value_without_newline);
		end if;
	end add_or_append_to_map;
begin
	-- put_line("gama_data directory: " & game_data);
	mkdir "--parents" "$games_dir";
	mkdir "--parents" "$maps_dir";

	put_line("Downloading '" & name & "' to '" & ofname & "' with tags '" & tags & " and comment " & comment);

	if strings.glob ("http*i", name) then
		put_line("Error: content name starts with http: '" & name & "'");
		return;
	end if;

	if not files.exists (fpath) then
		"$engine_dir/bin/pr-downloader" "--filesystem-writepath" "$game_data" "--download-map" "$name";
		--  wget --limit-rate=3000k -nc -O "$fname" -- "$url"


		if files.exists (fpath) then
			put_line ("Successfully downloaded '" & name & "' to '" & fname & "'");
		else
			put_line ("Error downloading '" & name & "' to '" & fname & "'");

			put_line ("Candidates:");
			find "$maps_dir" -printf "%T+ %p\n" | sort | tail -n 3;
		end if;

		put_line("rate limit sleep\n\n\n\n\n");
		sleep 3;

		changes_made := true;
	end if;


	-- Process tags - add map to per-tag lists
	nb_teams : integer := 2;
	tag : string;
	i : integer := 1;
	w, h : integer;
	loop
		tag := strings.field(tags, i, ' ');
		exit when tag = "";
		i := i + 1;
		put_line("Tag: " & tag);

		if strings.match ("[0-9]FFA", tag) then
			nb_teams := numerics.value(strings.head(tag, 1));
		end if;

		if strings.match ("[0-9]+x[0-9]+", tag) then
			w := numerics.value (strings.field(tag, 1, 'x'));
			h := numerics.value (strings.field(tag, 2, 'x'));
			--put_line("Map size: " & w & " times " & h);
		else
			add_or_append_to_map(tag, name);
		end if;
	end loop;

	put_line("nbteams: " & strings.image(nb_teams));
	if nb_teams = 2 then
		-- [0, 8] -> 1v1,  [8, 12] -> 2v2, [10, 16] -> 3v3, [12, 32] -> 4v4?
		if 0 <= w and w <= 8 then
			add_or_append_to_map("1v1", name);
		elsif 6 <= w and w <= 12 then
			add_or_append_to_map("2v2", name);
		elsif 10 <= w and w <= 16 then
			add_or_append_to_map("3v3", name);
		elsif 12 <= w and w <= 64 then
			add_or_append_to_map("4v4", name);
		end if;
	end if;

end add_content;


procedure add_initial_content (engine_dir : in string) is
	e : constant string := engine_dir;
	cm : boolean := false;
begin
	put_line("Processing initial content...");

	-- TODO:
	-- 16x16 https://springfiles.com/spring/spring-maps/ee-deltaglade-v02
	--download "ee-deltaglade-v02"              "-map 16x16 untested" \
	--  "https://springfiles.com/download/start/3553/9 https://springfiles.com/spring/spring-maps/ee-deltaglade-v02" EE-DeltaGlade-v02.sd7

	--download "Two Continents Remake v4"       "-map broken no_spawn" "https://springfiles.com/downloadmain/start/24436" "two_continents4.sdz"
	--download "Grts_Messa 008" "-map untested" \
	--  "https://springfiles.com/download/start/2192/9 https://springfiles.com/spring/spring-maps/grtsmessa" Grts_Messa_008.sd7
	--download "https://springfiles.com/downloadmain/start/18983"     bananadunes.sd7
	--download "Trees features for Spring:1944 1.0" "dependency" \
	--  "https://springfiles.com/spring/tools/trees-features-spring1944 https://springfiles.com/downloadmain/start/22783" "s44trees-v1.0.sd7" -- req for westerplatte
	--download "Houses features for Spring:1944 1.1" "dependency" \
	--  "https://springfiles.com/spring/tools/houses-features-spring1944 https://springfiles.com/downloadmain/start/22779" "s44houses-v1.1.sd7" -- req for westerplatte
	--download "1944 - Westerplatte 1.3"      "16x32 land naval s44" "https://springfiles.com/downloadmain/start/22787" "1944_-_westerplatte_1.3.sd7"
	-- omitted, hard to land
	--download "Trinity"                      "16x16 naval only flat s44" "https://springfiles.com/spring/spring-maps/trinity" "Trinity.sd7"
	-- bad, some weird sound gadget? https://springfiles.com/spring/spring-maps/desert
	--download "Aum"                          "18x18 land flat 5FFA s44" \
	--  "https://springfiles.com/spring/spring-maps/aum https://springfiles.com/downloadmain/start/18524" "aum_1.1.sd7"
	-- broken?!
	--download "Cany Waters Beta"            "??"  \
	--  "https://springfiles.com/spring/spring-maps/cany-waters-beta https://springfiles.com/download/start/6240/9" "cany_waters_beta.sd7"
	-- Would need a flag profile
	--download "FolsomDamDeluxeV4"            "14x20 land naval flat s44" "" "folsomdamdeluxev4.sd7"
	--download "ocean_view" "" "" "unknown"
	-- 4x4 https://springfiles.com/spring/spring-maps/1944bloodbay
	--download "https://springfiles.com/downloadmain/start/18409"     1944_bloodbay.sd7
	-- unknown https://springfiles.com/spring/spring-maps/bananadunes

	add_content(cm, e, "Aberdeen3v3v3",                    "map 16x16 land_only flat hilly s44 3FFA", "aberdeen3v3v3.sd7", "");
	add_content(cm, e, "aftershock",                       "map 14x26 land naval hilly s44", "aftershock.sd7", "");
	add_content(cm, e, "Crossing_4_final",                 "map 8x16 land_only flat s44", "crossing_4_final.sd7", "");
	add_content(cm, e, "FrostBiteV2",                      "map 8x16 land_only flat hilly s44", "frostbitev2.sd7", "");

	add_content(cm, e, "1944_Antwerp_Alpha2",              "map 30x30 land_only hilly s44", "1944_Antwerp_Alpha2.sd7",
	  "https://springfiles.com/spring/spring-maps/1944antwerpalpha2 https://springfiles.com/downloadmain/start/1680");
	add_content(cm, e, "1944_BocageSkirmish",              "map 8x8 land_only flat s44", "1944_BocageSkirmish.sd7",
	  "https://springfiles.com/spring/spring-maps/1944bocageskirmish https://springfiles.com/download/start/4409/9");
	-- 8x8 https://springfiles.com/spring/spring-maps/1944bocagesmall
	-- do not use http://spring1944.net/files/Maps/map_blacklist.html
	--download "https://springfiles.com/download/start/3961/9"        1944_BocageSmall.sd7

	add_content(cm, e, "1944_Caucasus_Skirmish_V4",        "map 6x6 land_only flat s44", "1944_Caucasus_Skirmish_V4.sd7",
	  "https://springfiles.com/spring/spring-maps/1944caucasusskirmishv4 https://springfiles.com/download/start/4407/9");
	add_content(cm, e, "1944_Cooper_Hill_v3",              "map 8x8 land_only hilly s44", "1944_cooper_hill_v3.sd7",
	  "https://springfiles.com/downloadmain/start/18962");

	add_content(cm, e, "1944_FloodBank_v0",                "map 12x16 land flat s44", "1944_FloodBank_v0.sd7",
	  "http://spring1944.net/files/Maps/1944_FloodBank_v0.sd7");

	add_content(cm, e, "1944_Hill_128",                    "map 20x20 land_only hilly s44 4FFA", "1944_Hill_128.sd7",
	  "https://springfiles.com/spring/spring-maps/1944-hill-128 https://springfiles.com/downloadmain/start/1678");

	add_content(cm, e, "1944_Keep_Off_The_Grass V1",       "map 10x12 land_only flat hilly s44", "1944_keep_off_the_grass_v1.sdz",
	  "https://springfiles.com/download/start/7683/9");
	add_content(cm, e, "1944_Keep_Off_The_Grass V2",       "map 10x12 land_only flat hilly s44", "1944_keep_off_the_grass_v2.sd7",
	  "https://springfiles.com/downloadmain/start/26033");
	add_content(cm, e, "1944_Kiev_V4",                     "map 12x18 land_only flat s44", "1944_Kiev_V4.sd7",
	  "https://springfiles.com/spring/spring-maps/1944kievv4 https://springfiles.com/downloadmain/start/1398");

	add_content(cm, e, "1944_Liege_V2",                    "map 30x30 land_only hilly s44", "1944_Liege_V2.sd7",
	  "https://springfiles.com/spring/spring-maps/1944-liege-v2 https://springfiles.com/downloadmain/start/1681");

	add_content(cm, e, "1944_Moro_River_V1",               "map 20x20 land_only hilly 3FFA 4FFA s44", "1944_Moro_River_V1.sd7",
	  "https://springfiles.com/spring/spring-maps/1944mororiverv1 https://springfiles.com/download/start/2280/9");

	add_content(cm, e, "1944_Plateau 1.0",                 "map 18x20 land_only hilly s44", "1944_plateau_1.0.sdz",
	  "https://springfiles.com/downloadmain/start/20203");
	add_content(cm, e, "1944_Plateau V2",                  "map 18x20 land_only hilly s44", "1944_plateau_v2.sd7",
	  "https://springfiles.com/download/start/8242/9");
	--download "1944_Prokhorovka"             "18x18 land only flat s44" "" "1944_Prokhorovka.sd7"
	add_content(cm, e, "1944_Prokhorovka_V2",              "map 18x18 land_only flat s44", "1944_Prokhorovka_V2.sd7",
	  "http://spring1944.net/files/Maps/1944_Prokhorovka_V2.sd7");
	--download "Prokhorovka_1944"             "18x18 land only flat s44" \
	--  "https://springfiles.com/spring/spring-maps/prokhorovka1944 https://springfiles.com/download/start/4490/9" "Prokhorovka_1944.sd7"

	add_content(cm, e, "1944_Red_Planet",                  "map 8x8 land_only flat s44", "1944_Red_Planet.sd7",
	  "https://springfiles.com/spring/spring-maps/1944-red-planet-remake https://springfiles.com/download/start/2305/9");
	add_content(cm, e, "1944_River_Valley_v4",             "map 30x30 land_only hilly s44", "1944_river_valley_v4.sdz",
	  "https://springfiles.com/spring/spring-maps/1944rivervalleyv4 https://springfiles.com/downloadmain/start/14101");
	add_content(cm, e, "1944_Road_To_Rome_V3",             "map 18x18 land_only hilly s44 4FFA", "1944_Road_To_Rome_V3.sd7",
	  "https://springfiles.com/spring/spring-maps/1944roadtoromev3 https://springfiles.com/downloadmain/start/1396");

	add_content(cm, e, "1944_Terra_Firma",                 "map 12x14 land_only flat s44", "1944_Terra_Firma.sd7",
	  "https://springfiles.com/spring/spring-maps/1944terrafirma https://springfiles.com/downloadmain/start/1417");
	add_content(cm, e, "1944_Titan",                       "map 12x18 land_only flat s44", "1944_Titan.sd7",
	  "https://springfiles.com/spring/spring-maps/1944-titan-v1 https://springfiles.com/download/start/4275/9");

	-- Where is v1?
	-- [Error] pr-downloader/src/main.cpp:157:main(): No map found for 1944_Village_Crossing
	--download "1944_Village_Crossing"        "8x8 land only flat s44" "" "1944_Village_Crossing.sd7"
	add_content(cm, e, "1944_Village_Crossing_v2",         "map 8x8 land_only flat s44", "1944_Village_Crossing_V2.sd7",
	  "https://springfiles.com/spring/spring-maps/1944villagecrossingv2 https://springfiles.com/download/start/4404/9");

	add_content(cm, e, "2_Mountains_Battlefield",          "map 16x16 land hilly s44 4FFA", "2_Mountains_Battlefield.sd7",
	  "https://springfiles.com/downloadmain/start/1971");

	add_content(cm, e, "AavikkoV2",                        "map 20x20 land_only flat s44", "AavikkoV2.sd7",
	  "https://springfiles.com/spring/spring-maps/aavikkov2 https://springfiles.com/downloadmain/start/2019");
	add_content(cm, e, "Adansonia v4.1",                   "map 14x16 land flat s44", "adansonia_v4.1.sd7",
	  "https://springfiles.com/spring/spring-maps/adansonia-0 https://springfiles.com/downloadmain/start/21423");
	add_content(cm, e, "Al Anih - Ila Tion 1.2",           "map 14x16 land naval flat s44", "al_anih_-_ila_tion_1.2.sd7",
	  "https://springfiles.com/spring/spring-maps/al-anih-ila-tion https://springfiles.com/downloadmain/start/17853");
	add_content(cm, e, "Alpine way v3",                    "map 16x16 land_only hilly s44", "Alpine_way_v3.sd7",
	  "https://springfiles.com/spring/spring-maps/alpine-way-v3 https://springfiles.com/downloadmain/start/1234");
	add_content(cm, e, "Amarante v3",                      "map 10x20 land hilly s44", "amarante_v3.sd7",
	  "https://springfiles.com/spring/spring-maps/amarante https://springfiles.com/download/start/7374/9");
	add_content(cm, e, "AmazonDeltav20",                   "map 16x16 land naval flat s44", "AmazonDeltav20.sd7",
	  "https://springfiles.com/spring/spring-maps/amazon-river-delta-v20 https://springfiles.com/download/start/4672/9");
	add_content(cm, e, "anyville 20190106",                "map 24x32 land_only hilly s44", "anyville_20190106.sdz",
	  "https://springfiles.com/downloadmain/start/25295");
	add_content(cm, e, "anyville 20190113",                "map 24x32 land_only hilly s44", "anyville_20190113.sdz",
	  "https://springfiles.com/downloadmain/start/25337");
	add_content(cm, e, "Aquatic Divide Revised v02",       "map 8x8 land flat s44", "aquatic_divide_revised_v02.sd7",
	  "https://springfiles.com/spring/spring-maps/aquatic-divide-revised-v02 https://springfiles.com/download/start/5676/9");
	add_content(cm, e, "Archers_Valley_v5",                "map 12x12 land_only hilly s44", "Archers_Valley_v5.sd7",
	  "https://springfiles.com/download/start/3867/9");
	add_content(cm, e, "Archipelago-v1",                   "map 10x20 naval_only flat s44", "Archipelago-v1.sd7", "");
	add_content(cm, e, "ArcticZoneV3",                     "map 32x32 naval_only flat s44", "ArcticZoneV3.sd7",
	  "https://springfiles.com/spring/spring-maps/arcticzonev3 https://springfiles.com/downloadmain/start/2069");
	add_content(cm, e, "Avalanche v3.1",                   "map 8x8 land_only hilly s44", "avalanche_v3.1.sd7",
	  "https://springfiles.com/spring/spring-maps/avalanche https://springfiles.com/downloadmain/start/22002");

	add_content(cm, e, "Badlands",                         "map 8x8 land_only hilly s44 tc", "badlands.sd7",
	  "https://springfiles.com/spring/spring-maps/badlands-0 https://springfiles.com/downloadmain/start/21732");
	add_content(cm, e, "Bandit_Plains_v1",                 "map 16x16 land_only s44", "Bandit_plains_v1.sd7",
	  "https://springfiles.com/spring/spring-maps/banditplainsv01 https://springfiles.com/downloadmain/start/1783");
	add_content(cm, e, "Barbary Coves v2",                 "map 16x18 naval_only flat s44", "barbary_coves_v2.sd7",
	  "https://springfiles.com/spring/spring-maps/barbary-coves https://springfiles.com/downloadmain/start/17534");
	add_content(cm, e, "BarracudaBay",                     "map 12x12 naval flat s44", "BarracudaBay.sd7", "https://springfiles.com/downloadmain/start/1187");
	add_content(cm, e, "Barren",                           "map 8x8 land_only hilly s44 tc", "barren.sd7",
	  "https://springfiles.com/spring/spring-maps/barren-0 https://springfiles.com/download/start/7943/9");
	add_content(cm, e, "Bilateral",                        "map 32x32 naval_only flat s44", "bilateral.sdz",
	  "https://springfiles.com/node/8979 https://springfiles.com/downloadmain/start/18549");
	add_content(cm, e, "Blindside_v2",                     "map 16x32 land hilly s44", "Blindside_v2.sd7", "https://springfiles.com/downloadmain/start/3947");

	add_content(cm, e, "Cervino v1",                       "map 12x20 land_only hilly s44", "Cervino_v1.sd7", "https://springfiles.com/downloadmain/start/22945");
	add_content(cm, e, "Charlie in the Hills v3.3",        "map 16x16 land flat s44", "CHARLIE_IN_THE_HILLS_V3.3.SD7",
	  "https://springfiles.com/downloadmain/start/17964");
	add_content(cm, e, "Comet Catcher Redux",              "map 12x16 land_only flat s44", "comet_catcher_redux.sd7", "");
	add_content(cm, e, "Conquest_of_Paradise_v1",          "map 16x16 land_only hilly s44", "Conquest_of_Paradise_v1.sd7",
	  "https://springfiles.com/downloadmain/start/1470");
	add_content(cm, e, "ConquerorsIsle-v02",               "map 24x24 land naval flat s44", "ConquerorsIsle-v02.sd7",
	  "https://springfiles.com/spring/spring-maps/conquerorsisle-v02 https://springfiles.com/download/start/3714/9");
	add_content(cm, e, "County_Crossing-v06",              "map 12x12 land_only hilly s44", "County_Crossing-v06.sd7",
	  "https://springfiles.com/downloadmain/start/1814");
	add_content(cm, e, "Cull",                             "map 12x12 naval_only s44 4FFA", "cull.sd7", "https://springfiles.com/downloadmain/start/3717");

	--download "DeltaSiegeDry"                "10x20 land only hilly s44" "https://springfiles.com/downloadmain/start/2315" "DeltaSiegeDry.sd7"
	add_content(cm, e, "DeltaSiegeDry_v3",                 "map 10x20 land_only hilly s44", "deltasiegedry_v3.sd7", "");
	add_content(cm, e, "DeltaSiege",                       "map 10x20 land naval hilly s44", "deltasiege.sd7", "https://springfiles.com/downloadmain/start/2321");
	-- Mega porc fest
	--download "Deserted_Gully-v05"           "12x20 land only hilly s44" "" "Deserted_Gully-v05.sd7"
	add_content(cm, e, "DianaBeachV2",                     "map 16x16 naval land flat 4FFA s44", "DianaBeachV2.sd7",
	  "https://springfiles.com/downloadmain/start/3696");
	add_content(cm, e, "DireStrights",                     "map 12x36 land naval flat s44", "direstrights.sd7", "");
	add_content(cm, e, "Downpour 1.1",                     "map 14x14 land naval flat hilly 3FFA s44", "downpour_1.1.sd7",
	  "https://springfiles.com/downloadmain/start/22810");
	add_content(cm, e, "Downs_of_Destruction_Fix",         "map 16x16 naval_only hilly s44", "downs_of_destruction_fix.sd7", "");
	add_content(cm, e, "Dworld_V1",                        "map 28x28 land naval s44", "Dworld_V1.sd7",
	  "https://springfiles.com/spring/spring-maps/dworldv1 https://springfiles.com/downloadmain/start/1433");

	add_content(cm, e, "Eisenwald v1.0",                   "map 16x16 land_only flat hilly s44", "eisenwald_v1.0.sd7",
	  "https://springfiles.com/spring/spring-maps/eisenwald https://springfiles.com/downloadmain/start/21834");
	add_content(cm, e, "Europe_a2",                        "map 16x32 land naval s44", "Europe_a2.sd7",
	  "https://springfiles.com/spring/spring-maps/europea2 https://springfiles.com/downloadmain/start/1712");
	add_content(cm, e, "Evergreen_Haven-v04",              "map 12x12 land_only hilly s44", "Evergreen_Haven-v04.sd7",
	  "https://springfiles.com/download/start/3501/9");
	add_content(cm, e, "Evolutionary",                     "map 4x10 land_only", "Evolutionary.sd7", "https://springfiles.com/downloadmain/start/2449");
	add_content(cm, e, "EvoRTS - Pockmark Valley - v12",   "map 16x16 land_only hilly s44", "evorts_-_pockmark_valley_-_v12.sd7",
	  "https://springfiles.com/spring/spring-maps/evolution-rts-pockmark-valley https://springfiles.com/downloadmain/start/16282");
	add_content(cm, e, "EvoRTS - Proving Grounds - v12",   "map 16x16 land naval hilly s44", "evorts_-_proving_grounds_-_v12.sd7",
	  "https://springfiles.com/spring/spring-maps/evolution-rts-proving-grounds https://springfiles.com/download/start/5604/9");
	add_content(cm, e, "EvoRTS - RiverGlade - v12",        "map 16x16 land naval flat s44", "evorts_-_riverglade_-_v12.sd7",
	  "https://springfiles.com/spring/spring-maps/evolution-rts-riverglade https://springfiles.com/downloadmain/start/16312");
	add_content(cm, e, "EvoRTS-Deserted_Gully-v01",        "map 12x20 land_only hilly s44", "EvoRTS-Deserted_Gully-v01.sd7",
	  "https://springfiles.com/spring/spring-maps/evolution-rts-deserted-gully Improved? version https://springfiles.com/downloadmain/start/1907");
	add_content(cm, e, "Expanded_Confluence",              "map 22x22 naval_only flat s44", "Expanded_Confluence.sd7",
	  "https://springfiles.com/spring/spring-maps/expanded-confluence https://springfiles.com/download/start/4095/9");
	add_content(cm, e, "Eye of Horus v13",                 "map 12x14 land_only flat mcl s44 tc temperate", "eye_of_horus_v13.sd7",
	  "https://springfiles.com/downloadmain/start/21519");

	add_content(cm, e, "Fairyland 1.31",                   "map 10x10 land hilly s44", "fairyland_1.31.sd7",
	  "https://springfiles.com/downloadmain/start/23003");
	add_content(cm, e, "Fetid Marsh - v06",                "map 16x16 land flat s44", "fetid_marsh_-_v06.sd7",
	  "https://springfiles.com/downloadmain/start/20528");
	add_content(cm, e, "Finns_Revenge",                    "map 12x12 land naval flat s44", "Finns_Revenge.sd7",
	  "https://springfiles.com/downloadmain/start/2471");
	add_content(cm, e, "Flooded Valley v2",                "map 10x10 naval_only hilly s44", "flooded_valley_v2.sd7",
	  "https://springfiles.com/spring/spring-maps/floodedvalleyv2 https://springfiles.com/downloadmain/start/3524");
	add_content(cm, e, "FolsomDamFlooded",                 "map 12x16 land naval flat s44", "FolsomDamFlooded.sd7",
	  "https://springfiles.com/download/start/4676/9");
	add_content(cm, e, "Fort_Delta",                       "map 12x20 land flat s44", "Fort_Delta.sd7", "https://springfiles.com/downloadmain/start/1649");
	add_content(cm, e, "FrozenFortress_v2",                "map 14x20 land_only flat s44", "FrozenFortress_v2.sd7",
	  "https://springfiles.com/spring/spring-maps/frozenfortressv2 https://springfiles.com/download/start/2226/9");

	add_content(cm, e, "GrassyPlain",                      "map 6x10 naval land s44", "GrassyPlain.sd7", "https://springfiles.com/downloadmain/start/2566");
	add_content(cm, e, "GreenValley",                      "map 16x16 land_only flat hilly s44", "GreenValley.sd7",
	  "https://springfiles.com/downloadmain/start/1386");
	add_content(cm, e, "Grts_Cookedwell_008",              "map 20x20 land_only hilly s44 4FFA", "Grts_CookedWell_008.sd7",
	  "https://springfiles.com/spring/spring-maps/grtscookedwell https://springfiles.com/download/start/2193/9");
	add_content(cm, e, "Grts_DesertValley_012",            "map 20x20 land_only hilly s44", "Grts_desertValley_012.sd7",
	  "https://springfiles.com/spring/spring-maps/grtsdesertvalley https://springfiles.com/download/start/2202/9");
	add_content(cm, e, "Grts_Northernmountains_009",       "map 20x20 land_only hilly s44", "grts_northernmountains_009.sd7",
	  "https://springfiles.com/spring/spring-maps/grtsnorthernmountains https://springfiles.com/download/start/2191/9");
	add_content(cm, e, "Grts_RiverValley_013",             "map 32x32 land flat s44", "Grts_RiverValley_013.sdz",
	  "https://springfiles.com/spring/spring-maps/grtsrivervalley https://springfiles.com/downloadmain/start/4047");
	add_content(cm, e, "Grts_Rocky_Glacier_010",           "map 12x24 land_only flat hilly s44", "grts_rocky_glacier_010.sd7",
	  "https://springfiles.com/spring/spring-maps/grtsrockyglacier https://springfiles.com/downloadmain/start/4097");

	add_content(cm, e, "Hide and Seek v06",                "map 14x14 land_only flat hilly s44", "hide_and_seek_v06.sd7",
	  "https://springfiles.com/downloadmain/start/22992");
	add_content(cm, e, "HighNoon-Redux-V3",                "map 16x16 naval_only flat s44", "HighNoon-Redux-V3.sd7",
	  "https://springfiles.com/downloadmain/start/2613");
	add_content(cm, e, "Highway 95 v5",                    "map 12x14 land_only flat s44", "highway_95_v5.sd7", "");
	add_content(cm, e, "HolyWater",                        "map 8x8 naval_only flat s44 3FFA", "HolyWater.sd7",
	  "https://springfiles.com/spring/spring-maps/holywater https://springfiles.com/downloadmain/start/1198");
	add_content(cm, e, "Hydra_Talus",                      "map 16x16 land_only hilly s44", "hydra_talus.sd7", "https://springfiles.com/downloadmain/start/16473");

	add_content(cm, e, "Ibex v1",                          "map 10x20 land flat hilly s44", "ibex_v1.sd7", "https://springfiles.com/downloadmain/start/21127");
	add_content(cm, e, "Icy Run v2",                       "map 4x12 land_only s44", "icy_run_v2.sd7", "https://springfiles.com/downloadmain/start/2655");
	add_content(cm, e, "IsisDelta_v02",                    "map 8x12 land flat s44", "isisdelta_v02.sd7", "");

	add_content(cm, e, "Kappa_Basin",                      "map 10x18 land_only flat s44", "kappa_basin.sd7", "");
	add_content(cm, e, "Kappa_Basin_Flooded_ZK-v07",       "map 10x18 naval flat s44", "kappa_basin_flooded_zk-v07.sd7",
	  "https://springfiles.com/spring/spring-maps/kappabasinfloodedzk-v07 https://springfiles.com/downloadmain/start/16119");
	add_content(cm, e, "Koom Valley V2",                   "map 16x24 land_only flat s44", "koom_valley_v2.sd7",
	  "https://springfiles.com/downloadmain/start/7381");

	add_content(cm, e, "LakeDay_v1",                       "map 8x8 land naval flat s44", "lakeday_v1.sd7",
	  "https://springfiles.com/spring/spring-maps/lakedayv1 https://springfiles.com/downloadmain/start/16636");
	add_content(cm, e, "LakeDay_v2",                       "map 20x20 naval hilly s44", "lakeday_v2.sd7",
	  "https://springfiles.com/spring/spring-maps/lakedayv2 https://springfiles.com/download/start/5562/9");
	add_content(cm, e, "LakeLargo_v1",                     "map 12x12 naval flat s44", "lakelargo_v1.sd7",
	  "https://springfiles.com/spring/spring-maps/lakelargo https://springfiles.com/downloadmain/start/19331");
	add_content(cm, e, "Living Lands 4.1",                 "map 8x8 land_only hilly s44", "living_lands_4.1.sd7",
	  "https://springfiles.com/downloadmain/start/23004");

	add_content(cm, e, "Malibu Beach v1",                  "map 10x20 land naval flat s44", "malibu_beach_v1.sd7",
	  "https://springfiles.com/spring/spring-maps/malibu-beach https://springfiles.com/download/start/5790/9");
	add_content(cm, e, "Mercurial v1.1",                   "map 12x12 land flat s44", "mercurial_v1.1.sd7",
	  "https://springfiles.com/spring/spring-maps/mercurial");
	add_content(cm, e, "MiniTabula-beta",                  "map 6x8 land_only flat s44", "minitabula-beta.sd7",
	  "https://springfiles.com/spring/spring-maps/minitabula https://springfiles.com/download/start/5640/9");
	add_content(cm, e, "monte",                            "map 20x20 land_only hilly 4FFA s44", "monte.sd7",
	  "https://springfiles.com/spring/spring-maps/1944montebeta https://springfiles.com/downloadmain/start/1677");
	add_content(cm, e, "MoonQ20x",                         "map 10x20 land_only hilly s44", "moonq20x.sd7", "");
	add_content(cm, e, "Mountain Passes V2",               "map 16x16 land naval hilly s44", "mountain_passes_v2.sd7",
	  "https://springfiles.com/downloadmain/start/17597");
	add_content(cm, e, "MountainGassLands_v1.4",           "map 8x8 land_only flat s44", "MountainGassLands_v1.4.sd7",
	  "https://springfiles.com/downloadmain/start/1669");

	-- Narrow valley omitted, too long
	add_content(cm, e, "neurope_a7",                       "map 16x32 land naval flat s44", "neurope_a7.sd7", "https://springfiles.com/downloadmain/start/3564");
	add_content(cm, e, "Nuclear_Winter_1944",              "map 12x20 land_only flat hilly s44", "Nuclear_Winter_1944.sd7",
	  "https://springfiles.com/spring/spring-maps/nuclearwinter1944 https://springfiles.com/downloadmain/start/1633");
	add_content(cm, e, "Nuclear Winter v2",                "map 12x20 land_only flat hilly s44", "Nuclear_Winter_v2.sd7",
	  "https://springfiles.com/downloadmain/start/22989");
	add_content(cm, e, "Nuclear Winter v3",                "map 12x20 land_only flat hilly s44", "Nuclear_Winter_v3.sd7",
	  "https://springfiles.com/downloadmain/start/22999");

	add_content(cm, e, "OkiRiver_v2",                      "map 20x20 land naval flat s44", "OkiRiver_v2.sd7", "https://springfiles.com/downloadmain/start/2893");
	add_content(cm, e, "Onyx Cauldron 1.7",                "map 18x18 land hilly mcl s44 tc cold", "onyx_cauldron_1.7.sd7",
	  "https://springfiles.com/download/start/7471/9");
	add_content(cm, e, "opcodehaven v1.2",                 "map 8x8 land naval flat s44", "opcodehaven_v1.2.sd7",
	  "https://springfiles.com/downloadmain/start/24880");

	add_content(cm, e, "Paradise Lost v2",                 "map 10x20 naval_only flat s44", "paradise_lost_v2.sd7",
	  "https://springfiles.com/spring/spring-maps/paradise-lost-v2 https://springfiles.com/download/start/4444/9");
	add_content(cm, e, "Pearl Springs v2",                 "map 14x24 land flat s44", "pearl_springs_v2.sd7",
	  "https://springfiles.com/spring/spring-maps/pearl-springs https://springfiles.com/download/start/5856/9");

	add_content(cm, e, "River_Dale_Remake-v01",            "map 12x20 land_only flat hilly s44", "River_Dale_Remake-v01.sd7",
	  "https://springfiles.com/spring/spring-maps/riverdale-remake-v01 https://springfiles.com/download/start/4157/9");
	add_content(cm, e, "rysia",                            "map 24x24 land_only hilly s44", "rysia.sd7",
	  "https://springfiles.com/spring/spring-maps/rysia https://springfiles.com/downloadmain/start/3580");
	add_content(cm, e, "Real Europe V4",                   "map 24x24 land naval hilly s44", "real_europe_v4.sdz",
	  "https://springfiles.com/spring/spring-maps/real-europe");

	add_content(cm, e, "SailAway",                         "map 10x20 naval_only s44", "SailAway.sd7", "");
	-- download "ScorpioBattleground" omitted
	add_content(cm, e, "SapphireShores_Dry_V2.1",          "map 8x18 land_only", "sapphireshores_dry_v2.1.sd7",
	  "https://springfiles.com/spring/spring-maps/sapphire-shores-dry");
	add_content(cm, e, "Sea of Dunes v1.0",                "map 20x20 land_only hilly s44", "sea_of_dunes_v1.0.sd7",
	  "https://springfiles.com/spring/spring-maps/sea-dunes");
	add_content(cm, e, "Seatofpower_1.3_",                 "map 16x16 naval land hilly 4FFA s44", "Seatofpower_1.3_.sd7",
	  "https://springfiles.com/downloadmain/start/1048");
	add_content(cm, e, "Shimmershore v1.0",                "map 12x12 naval_only flat s44", "shimmershore_v1.0.sd7",
	  "https://springfiles.com/downloadmain/start/22857");
	add_content(cm, e, "Sierra-v2",                        "map 14x14 land_only hilly s44", "Sierra-v2.sd7", "https://springfiles.com/spring/spring-maps/sierra-v2");
	add_content(cm, e, "Simple 6 Way 1.2-KotH",            "map 18x18 land flat 3FFA s44", "simple_6_way_1.2-koth.sd7",
	  "https://springfiles.com/download/start/8095/9");
	add_content(cm, e, "Small Supreme Battlefield V2",     "map 16x16 land naval flat s44", "Small_Supreme_Battlefield_V2.sd7",
	  "https://springfiles.com/spring/spring-maps/smallsupremebattlefieldv2 https://springfiles.com/downloadmain/start/3121");
	add_content(cm, e, "Small Supreme Islands V2",         "map 16x16 naval_only flat s44", "Small_Supreme_Islands_v2.sd7",
	  "https://springfiles.com/spring/spring-maps/smallsupremeislandsv2 https://springfiles.com/downloadmain/start/3125");
	add_content(cm, e, "SmallDivide",                      "map 8x8 land_only hilly s44", "SmallDivide.sd7", "https://springfiles.com/downloadmain/start/3109");
	add_content(cm, e, "SnakeIslandV2",                    "map 16x16 land naval flat s44", "SnakeIslandV2.sd7",
	  "https://springfiles.com/downloadmain/start/1170");
	add_content(cm, e, "spiders web",                      "map 14x18 land flat hilly s44", "spiders_web.sd7",
	  "https://springfiles.com/spring/spring-maps/spiders-web https://springfiles.com/downloadmain/start/17021");
	add_content(cm, e, "SplinteredTropicsV3",              "map 16x16 naval_only flat s44", "splinteredtropicsv3.sd7",
	  "https://springfiles.com/spring/spring-maps/splinteredtropicsv3 https://springfiles.com/downloadmain/start/16171");
	add_content(cm, e, "SpringMountainDelta",              "map 16x16 land naval flat hilly s44", "springmountaindelta.sd7",
	  "https://springfiles.com/spring/spring-maps/spring-mountain-delta-v1 https://springfiles.com/download/start/4660/9");
	-- toooo much income, tooo many flags!
	--download "summer_dream_v0"              "14x38 land only flat hilly s44" "" "summer-dream.sd7"

	add_content(cm, e, "Tabula_Flooded_v04",               "map 14x16 naval_only flat s44", "tabula_flooded_v04.sd7",
	  "https://springfiles.com/download/start/5358/9");
	add_content(cm, e, "Tabula-v4",                        "map 14x16 land_only flat s44", "tabula-v4.sd7", "https://springfiles.com/download/start/1474/9");
	add_content(cm, e, "Talus",                            "map 16x16 land_only flat s44", "Talus.sd7", "https://springfiles.com/download/start/4353/9");
	add_content(cm, e, "Tangerine",                        "map 16x16 land naval flat s44", "tangerine.sd7",
	  "https://springfiles.com/spring/spring-maps/tangerine-fixed https://springfiles.com/download/start/2702/9");
	add_content(cm, e, "Tau10Dry",                         "map 16x16 land_only flat s44", "tau10dry.sdz", "");
	add_content(cm, e, "Tempest",                          "map 20x20 land naval flat s44", "Tempest.sd7", "https://springfiles.com/downloadmain/start/3300");
	add_content(cm, e, "Tempest Siege v1",                 "map 10x20 land_only flat s44", "tempest_siege_v1.sd7",
	  "https://springfiles.com/spring/spring-maps/tempest-siege https://springfiles.com/downloadmain/start/16420");
	add_content(cm, e, "Temple Redux v1.2",                "map 12x12 land naval flat s44", "temple_redux_v1.2.sd7",
	  "https://springfiles.com/downloadmain/start/21108");
	add_content(cm, e, "Ternion",                          "map 12x12 3FFA land naval s44", "Ternion.sd7", "https://springfiles.com/download/start/2387/9");
	add_content(cm, e, "The river Nix 20",                 "map 24x24 land flat hilly s44", "the_river_nix_20.sd7",
	  "https://springfiles.com/spring/spring-maps/river-nix https://springfiles.com/download/start/5839/9");
	add_content(cm, e, "The_Road_Final",                   "map 16x16 land naval flat s44", "The_Road_Final.sd7",
	  "https://springfiles.com/downloadmain/start/1873");
	add_content(cm, e, "TheRockFinal",                     "map 16x20 land naval hilly s44", "TheRockFinal.sd7",
	  "https://springfiles.com/downloadmain/start/3615");
	add_content(cm, e, "Thornford 2",                      "map 14x14 land_only flat s44", "thornford_2.sd7", "https://springfiles.com/downloadmain/start/25092");
	add_content(cm, e, "ThreeReefs16",                     "map 16x16 land naval hilly s44 3FFA", "ThreeReefs16.sd7",
	  "https://api.springfiles.com/files/maps/threereefs16.sd7 https://springfiles.com/downloadmain/start/3899");
	add_content(cm, e, "TinySkirmishRedux1.1",             "map 4x4 land_only flat s44", "tinyskirmishredux1.1.sd7", "");
	add_content(cm, e, "Titan-Flooded v1",                 "map 12x18 naval_only flat hilly s44", "titan-flooded_v1.sd7",
	  "https://springfiles.com/spring/spring-maps/titan-flooded https://springfiles.com/download/start/8057/9");
	add_content(cm, e, "Torn Islands Remake",              "map 16x16 land naval flat s44", "torn_islands_remake.sd7",
	  "https://springfiles.com/spring/spring-maps/torn-islands-remake https://springfiles.com/download/start/4694/9");
	add_content(cm, e, "Tropicalo",                        "map 10x10 land hilly s44", "Tropicalo.sd7",
	  "https://springfiles.com/spring/spring-maps/tropicalo https://springfiles.com/downloadmain/start/973");
	add_content(cm, e, "Tundra",                           "map 16x16 land flat s44", "Tundra.sdz", "https://springfiles.com/download/start/4378/9");

	add_content(cm, e, "unified_battlefield_v0",           "map 16x16 land naval flat hilly s44", "unified_battlefield_v0.sd7",
	  "https://springfiles.com/spring/spring-maps/unifiedbattlefieldv0 https://springfiles.com/download/start/2405/9");

	add_content(cm, e, "Verdant 1944",                     "map 16x16 land hilly s44", "verdant_1944.sd7", "http://spring1944.net/files/Maps/Verdant_1944.sd7");

	add_content(cm, e, "Water-Ways",                       "map 12x12 naval_only flat s44", "Water-Ways.sd7",
	  "https://springfiles.com/spring/spring-maps/water-ways https://springfiles.com/downloadmain/start/3426");
	add_content(cm, e, "WesternFrontierV2",                "map 12x18 land_only flat hilly s44", "WesternFrontierV2.sd7",
	  "https://springfiles.com/spring/spring-maps/westernfrontierv2 https://springfiles.com/downloadmain/start/3897");
	add_content(cm, e, "Wide Pass Final",                  "map 12x20 land_only flat hilly s44", "wide_pass_final.sd7",
	  "https://springfiles.com/spring/spring-maps/wide-pass-final https://springfiles.com/download/start/4813/9");
	add_content(cm, e, "Wide Pass-Modless_v0.1",           "map 12x20 land_only flat hilly s44", "wide_pass-modless_v0.1.sd7",
	  "https://springfiles.com/download/start/2205/9");




	add_content(cm, e, "ArcticPlainsV2.1",                 "map 16x16 land_only flat hilly tc", "ArcticPlainsV2.1.sdz",
	  "https://springfiles.com/downloadmain/start/1009");
	add_content(cm, e, "Iceland_v1",                       "map 16x16 land flat mcl tc cold", "iceland_v1.sdz", "https://springfiles.com/downloadmain/start/17289");
	add_content(cm, e, "Mescaline_V2",                     "map 12x20 land_only hilly tc", "Mescaline_v2.sd7", "https://springfiles.com/downloadmain/start/4078");
	add_content(cm, e, "Ravaged_v2",                       "map 10x10 land flat mcl tc temperate", "ravaged_v2.sd7", "https://springfiles.com/downloadmain/start/5275");
	add_content(cm, e, "Wanderlust v03",                   "map 8x10 land_only hilly tc", "wanderlust_v03.sd7",
	  "https://springfiles.com/download/start/8083/9");




	add_content(cm, e, "Central Hub",                      "map 8x8 kp", "central_hub.sd7", "");
	add_content(cm, e, "Corrupted Core",                   "map 4x4 kp", "corrupted_core.sd7", "");
	add_content(cm, e, "Data Cache L1",                    "map 6x8 kp", "Data_Cache_L1.sd7", "");
	add_content(cm, e, "DigitalDivide_PT2",                "map 6x6 kp", "DigitalDivide_PT2.sd7", "");
	add_content(cm, e, "Direct Memory Access 0.5c (beta)", "map 12x16 kp", "direct_memory_access_0.5c__beta_.sd7", "");
	add_content(cm, e, "Direct Memory Access 0.5e (beta)", "map 12x16 kp", "direct_memory_access_0.5e__beta_.sd7", "");
	add_content(cm, e, "Dual Core",                        "map 4x8 kp", "Dual_Core.sd7", "");
	add_content(cm, e, "Hex Farm 8",                       "map 24x24 kp", "Hex_Farm_8.sd7", "");
	add_content(cm, e, "Major_Madness3.0",                 "map 10x10 kp", "major_madness3.0.sd7", "");
	add_content(cm, e, "Marble_Madness_Map",               "map 4x4 kp", "Marble_Madness_Map.sd7", "");
	-- No map found for the following:
	--download "Memory Bank 0.3"              "16x16 kp" "" "Memory_Bank_v3.sdz"
	add_content(cm, e, "pacman",                           "map 8x8 kp", "pacman.sd7", "");
	add_content(cm, e, "Palladium 0.5 (beta)",             "map 10x10 kp", "palladium_0.5__beta_.sd7", "");
	add_content(cm, e, "Quad Core",                        "map 8x8 kp", "Quad_Core.sd7", "");
	add_content(cm, e, "Speed_Balls_16_Way",               "map 10x10 kp", "Speed_Balls_16_Way.sdz", "");
	add_content(cm, e, "Spooler Buffer 0.5 (beta)",        "map 2x4 kp", "spooler_buffer_0.5__beta_.sd7", "");



 -- new for MCL
	add_content(cm, e, "Aberdeen_1.1",                     "map 16x16 land_only flat hilly mcl 3FFA temperate", "aberdeen_1.1.sd7", "https://springfiles.com/downloadmain/1131");
	add_content(cm, e, "Centerrock Remake 1.2",            "map 16x16 land_only hilly mcl hot", "centerrock_remake_1.2.sd7", "https://springfiles.com/downloadmain/33317");
	add_content(cm, e, "DesertSiege_v2b",                  "map 20x12 land_only hilly mcl hot", "DesertSiege_v2b.sd7", "");
	add_content(cm, e, "DSDR 3.98",                        "map 20x12 land_only hilly mcl hot", "dsdr_3.98.sd7", "");
	add_content(cm, e, "Green River Confluence v10",       "map 16x16 land hilly mcl temperate", "green_river_confluence_v10.sd7", "");
	add_content(cm, e, "Melt_V2",                          "map 16x16 land_only hilly mcl cold", "melt_v2.sd7", "");
	add_content(cm, e, "Mercury_v1",                       "map 16x16 land_only hilly mcl hot", "Mercury_v1.sd7", "");
	add_content(cm, e, "Moor_v4",                          "map 24x24 land_only hilly mcl temperate", "moor_v4.sdz", "");
	add_content(cm, e, "Red Comet Remake 1.8",             "map 12x8 land_only hilly mcl cold", "red_comet_remake_1.8.sd7", "");
	add_content(cm, e, "RustyDelta_Final",                 "map 20x8 land_only hilly mcl cold", "RustyDelta_Final.sd7", "");
	add_content(cm, e, "Sandcastles2Big",                  "map 24x24 land_only hilly mcl temperate", "sandcastles2big.sd7", "");
	add_content(cm, e, "Seth's Ravine 3.1",                "map 16x12 land_only hilly mcl temperate", "seth_s_ravine_3.1.sd7", "");
	add_content(cm, e, "Stronghold V4",                    "map 16x16 land_only hilly mcl temperate", "stronghold_v4.sd7", "");


	--download "https://springfiles.com/downloadmain/start/20195"     "S44_equilibrio_2.092.sdz"
	--download "https://springfiles.com/downloadmain/start/20396"     "S44_equilibrio_2.094.sdz"
	--download "https://springfiles.com/downloadmain/start/20453"     "S44_equilibrio_2.095.sdz" # JAL fckd up
	--download "https://springfiles.com/downloadmain/start/20455"     "S44_equilibrio_2.0951.sdz"
	--download "https://springfiles.com/downloadmain/start/20480"     "S44_equilibrio_2.096.sdz"
	--download "https://springfiles.com/downloadmain/start/20483"     "S44_equilibrio_2.0962.sdz"
	--download "https://springfiles.com/downloadmain/start/20538"     "S44_equilibrio_2.097.sdz"
	--download "https://springfiles.com/downloadmain/start/20543"     "S44_equilibrio_2.0971.sdz"
	--download "https://springfiles.com/downloadmain/start/20567"     "S44_equilibrio_2.098.sdz"
	--download "https://springfiles.com/downloadmain/start/20635"     "S44_equilibrio_2.0982.sdz"
	--download "https://springfiles.com/downloadmain/start/20651"     "S44_equilibrio_2.09881.sdz"
	--download "https://springfiles.com/downloadmain/start/20654"     "S44_equilibrio_2.09882.sdz"
	--download "https://springfiles.com/downloadmain/start/20662"     "S44_equilibrio_2.09883.sdz"
	--https://springfiles.com/sites/default/files/downloads/spring/games/s44equilibrio2.1.sdz

	--pr-downloader --filesystem-writepath "$game_data" --rapid-download 's44:test'
	--pr-downloader --filesystem-writepath "$game_data" --rapid-download 's44:stable'
	--pr-downloader --filesystem-writepath "$game_data" --rapid-download 'tc:stable'
	--pr-downloader --filesystem-writepath "$game_data" --download-game 'Spring: 1944 v2.0'
	--pr-downloader --filesystem-writepath "$game_data" --download-game 'Spring: 1944 v3.01'


	put_line(".. done processing content.");
	put_line("Generating map lists...");

	put_line(dynamic_hash_tables.get(tagmap, "land_only"));

	declare
		ml : string := "[all]" & ASCII.LF & ".*" & ASCII.LF & ASCII.LF;

		procedure append_to_maplist (what : string) is
			maps : string := dynamic_hash_tables.get(tagmap, what);
		begin
			ml := ml & "[" & what & "]" & ASCII.LF;
			ml := ml & maps & ASCII.LF & ASCII.LF;
		end append_to_maplist;
	begin
		append_to_maplist("land_only");
		append_to_maplist("land");
		append_to_maplist("naval");
		append_to_maplist("naval_only");
		append_to_maplist("hilly");
		append_to_maplist("flat");
		append_to_maplist("3FFA");
		append_to_maplist("4FFA");

		append_to_maplist("hot");
		append_to_maplist("temperate");
		append_to_maplist("cold");

		append_to_maplist("mcl");
		append_to_maplist("s44");
		append_to_maplist("kp");
		append_to_maplist("tc");

		append_to_maplist("1v1");
		append_to_maplist("2v2");
		append_to_maplist("3v3");
		append_to_maplist("4v4");
		put_line(ml);

		if cm then --if changes made (new maps?)
			maplist_file : file_type;
			create (maplist_file, out_file, "etc/mapLists.conf");
			put(maplist_file, ml);
		end if;
	end;

end add_initial_content;
