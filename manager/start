#!/usr/bin/env spar

-- SPADS manager -- entry point
-- Copyright (C) 2015-2020 Fedja Beader
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Affero General Public License as
-- published by the Free Software Foundation, either version 3 of the
-- License, or (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Affero General Public License for more details.
--
-- You should have received a copy of the GNU Affero General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

pragma annotate (summary, "start");
pragma annotate (description, "Management script for SPADS (SpringRTS) autohosts");
pragma annotate (description, "  run me without arguments for usage help");
pragma annotate (author, "Fedja Beader");

procedure start is
	procedure print_usage is begin
		put_line ("Usage: " & command_line.argument(0) & " <autohost_name>");
	end print_usage;

	-- Spar will point it out more clearly
	NoEntryException : exception;
	FileSystemException : exception;
	GeneralErrorException : exception;

	spring_engines_dir : constant string := "/opt/springrts.com/spring/";
	home_dir            : constant string := "/data/users/s44";
	spads_home_dir      : constant string := home_dir & "/spads";

	-- it seems one cannot use variables in with separate statements
	with separate "./manager/autohost_definitions.sp";
	-- for passwords, use the same syntax as the rest of the get functions:
	--   function get_lobby_password (ahname : in string) return string is
	-- and additionally specify:
	--   replays_account  : constant string := "...";
	--   replays_password : constant string := "...";
	with separate "./manager/autohost_secrets.sp";

	with separate "./manager/install_spads_from_template.sp";

	with separate "./manager/initial_content.sp";
begin
	if command_line.argument_count /= 1 then
		print_usage;
		command_line.set_exit_status (1);
		return;
	end if;

	ah_name                    : string  := command_line.argument (1);

	ah_autoload_plugins        : string  := get_autoload_plugins        (ah_name);
	ah_autostop_condition      : string  := get_autostop_condition      (ah_name);
	ah_battle_name             : string  := get_battle_name             (ah_name);
	ah_battle_password         : string  := get_battle_password         (ah_name);
	ah_battle_preset           : string  := get_battle_preset           (ah_name);
	ah_control_port            : natural := get_control_port            (ah_name);
	ah_default_map             : string  := get_default_map             (ah_name);
	ah_engine_version          : string  := get_engine_version          (ah_name);
	ah_engine_type             : string  := get_engine_type             (ah_name);
	ah_game                    : string  := get_game                    (ah_name);
	ah_lobby_password          : string  := get_lobby_password          (ah_name);
	ah_map_list                : string  := get_map_list                (ah_name);
	ah_master_channel          : string  := get_master_channel          (ah_name);
	ah_welcome_message         : string  := get_welcome_message         (ah_name);
	ah_welcome_message_in_game : string  := get_welcome_message_in_game (ah_name);
	ah_end_game_arguments      : string  := get_end_game_arguments      (ah_name);
	ah_port                    : natural := get_port                    (ah_name);
	ah_promote_channels        : string  := get_promote_channels        (ah_name);

	put_line("Starting S44 autohost management script for: " & ah_name & " on port: " & strings.image(ah_port));
	put_line(" with autoload plugins:              " & ah_autoload_plugins);
	put_line(" with autostop condition:            " & ah_autostop_condition);
	put_line(" with battle name:                   " & ah_battle_name);
	put_line(" with battle password:               " & ah_battle_password);
	put_line(" with battle preset:                 " & ah_battle_preset);
	put_line(" with control port:                  " & strings.image (ah_control_port) );
	put_line(" with engine version:                " & ah_engine_version);
	put_line(" with engine type:                   " & ah_engine_type);
	put_line(" with game:                          " & ah_game);
	put_line(" with lobby password:                " & ah_lobby_password);
	put_line(" with map list:                      " & ah_map_list);
	put_line(" with default map:                   " & ah_default_map);
	put_line(" with master channel:                " & ah_master_channel);
	put_line(" with welcome message:               " & ah_welcome_message);
	put_line(" with welcome message in game:       " & ah_welcome_message_in_game);
	put_line(" with end game arguments:            " & ah_end_game_arguments);
	put_line(" with promote channels:              " & ah_promote_channels);

	autohost_var_dir    : constant string := spads_home_dir & "/var/" & ah_name;
	autohost_log_dir    : constant string := spads_home_dir & "/log/" & ah_name;

	-- ensure needed directories exist
	put_line ("Creating var dir: " & autohost_var_dir);
	mkdir -p "$autohost_var_dir";
	if $? /= 0 then raise FileSystemException; end if;

	put_line ("Creating log dir: " & autohost_log_dir);
	mkdir -p "$autohost_log_dir";
	if $? /= 0 then raise FileSystemException; end if;

	spring_engine_dir   : constant string := spring_engines_dir & ah_engine_version;
	-- "Spring engine installation directory %s does not exist!\n" "$spring_engine_dir"
	test -d "$spring_engine_dir";
	if $? /= 0 then raise FileSystemException; end if;


	-- Install SPADS for this engine if necessary
	spads_template_install_dir : constant string := spads_home_dir & "/install-template";
	spads_install_dir          : constant string := spads_home_dir & "/install/" & ah_engine_version;
	spring_unitsync_path       : constant string := spring_engine_dir & "/lib/libunitsync.so";
	spring_data_path           : constant string := spring_engine_dir & "/share/games/spring";

	test -d "$spads_install_dir";
	if $? /= 0 then
		install_spads_from_template (
		  spring_engine_dir,
		  spads_template_install_dir,
		  spads_install_dir,
		  spring_unitsync_path,
		  spring_data_path);
	end if;


	current_dir : constant string := PWD;

	add_initial_content(spring_engine_dir);


	put_line ("Changing directory to " & spads_install_dir & " in order to start SPADS");
	cd "$spads_install_dir";
	if $? /= 0 then raise FileSystemException; end if;

	perl ./spads.pl "$spads_home_dir/etc/spads.conf"
	  "autoStop=$ah_autostop_condition"
	  "defaultMap=$ah_default_map"
	  "managerMasterChannel=$ah_master_channel"
	  "mapList=$ah_map_list"
	  "NAME=$ah_name"
	  "PASS=$ah_lobby_password"
	  "BNAME=$ah_battle_name"
	  "MODNAME=$ah_game"
	  "BATTLE_PRESET=$ah_battle_preset"
	  "PORT=$ah_port"
	  "AHPORT=$ah_control_port"
	  "ENGINE=$ah_engine_version"
	  "engineType=$ah_engine_type"
	  "ENGINES_DIR=$spring_engines_dir"
	  "SPADS_PATH=$spads_install_dir" -- unused?
	  "AL_PLUGINS=$ah_autoload_plugins"
	  "welcomeMsg=$ah_welcome_message"
	  "welcomeMsgInGame=$ah_welcome_message_in_game"
	  "replaysPassword=$replays_password"
	  "replaysAccount=$replays_account"
	  "battlePassword=$ah_battle_password"
	  "endGameCommandArgs=$ah_end_game_arguments"
	  "PROMOTE_CHANNELS=$ah_promote_channels";

	cd "$current_dir";
	if $? /= 0 then raise FileSystemException; end if;

	command_line.set_exit_status (0);
end start;
